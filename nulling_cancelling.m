function resultado = nulling_cancelling(H, y)
  [Q,R] = qr(H);  
  % z = Rx+~n
  z = Q' * y;
  
  resultado = zeros(size(H, 2), 1);  
    
  for i = size(H, 2):-1:1
    if (real(z(i) / R(i, i))) >= 0
        resultado(i)= 1;
    else
        resultado(i)=-1;
    end
    
    z = z - resultado(i) * R(:,i);   
  end
end