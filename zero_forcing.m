function resultado = zero_forcing(H, y)
    pinv_H = pinv(H);
    
    resultado = pinv_H*y;
    for i = 1 : size(resultado)
        if real(resultado(i)) >= 0
            resultado(i) = 1;
        else
            resultado(i) = -1;
        end
    end        
end

