clear all;
close all;

%% Modelo do canal---------------------------------------------------------
% y = Hx + n
% ? NT antenas de transmissão, NR antenas derecepção
% ? x é o vetor coluna de NT símbolos enviados
% ? y é o vetor coluna de NR símbolos recebidos
% ? n é o vetor de ruído (NR valores)
% o H é uma matriz complexa, onde cada posição Hi, j indica defasamento e
% atenuação da antena de transmissão i até a antena de recepção j
% ? H, x, y e n são complexos
%% -------------------------------------------------------------------------
Nb = 300; % n�mero de bits enviados

Eb_N0_dB = 0:1:10; %faixa de Eb/N0 a ser simulada (em dB)
Eb_N0_lin = 10 .^ (Eb_N0_dB/10); %Eb/N0 linearizado
Eb = 1; %energia por s�mbolo � constante (1^2 = (-1)^2 = 1), 1 bit por s�mbolo (caso geral: energia m�dia por s�mbolo / bits por s�mbolo)
NP = Eb ./ (Eb_N0_lin); %pot�ncia do ru�do
NA = sqrt(NP); %amplitude � a raiz quadrada da pot�ncia

sets = [2,2;2,3;4,4]; %cenarios das simulacoes
size_sets = length(sets(:,1)); 
for k=1 : size_sets

NT = sets(k,1); %numero de antenas de transmissao (parametro solicitado no enunciado)
NR = sets(k,2); %numero de antenas de recepcao (parametro solicitado no enunciado)
H = zeros (NR, NT); %Matriz que simula o meio
% devera ser simualdo sistemas 2*2 2*3 e 4*4 (NT*NR)
x = zeros(NT,1); % vetor coluna de NT simbolos enviados
y = zeros(NR,1); % vetor coluna de NR simbolos recebidos
n = zeros(NR,1); % vetor de ruido (NR valores)

ber_zero = zeros(size(Eb_N0_lin)); %pre-allocates BER vector
ber_nulling = zeros(size(Eb_N0_lin)); %pre-allocates BER vector
ber_sorted = zeros(size(Eb_N0_lin)); %pre-allocates BER vector

for i = 1:length(Eb_N0_lin)
    zero_forcing_erros = 0;
    nulling_cancelling_erros = 0;
    sort_nulling_cancelling_erros = 0;
    
    for j = 1:Nb
        x = 2*randi(2, NT, 1)-3;%vetor coluna complexo com parte real {-1, 1} e parte imagin�ria = 0
        n = NA(i)*complex(randn(NR, 1), randn(NR, 1))*sqrt(0.5);

        % altera matriz H a cada 5 envios
        if (mod(j-1, 5) == 0)
            H = complex(randn(NR, NT), randn(NR, NT));
        end
        %calcula o y (s�mbolos recebidos)
        y = H*x+n;        

        % detec��o de s�mbolos utilizando o m�todo zero_forcing
        zero_forcing_x = zero_forcing(H, y);
        zero_forcing_erros = sum(zero_forcing_x ~= x) + zero_forcing_erros;

        % detec��o de s�mbolos utilizando o m�todo nulling and cancelling
        nulling_cancelling_x = nulling_cancelling(H, y);
        nulling_cancelling_erros = sum(nulling_cancelling_x ~= x) + nulling_cancelling_erros;

        % detec��o de s�mbolos utilizando o m�todo sorted nulling and cancelling
        s_nulling_cancelling_x = sorted_nulling_cancelling(x, H, y);
        sort_nulling_cancelling_erros = sum(s_nulling_cancelling_x ~= x) + sort_nulling_cancelling_erros;
    end
    
    % calcula ber
    ber_zero(i) = zero_forcing_erros/Nb;
    ber_nulling(i) = nulling_cancelling_erros/Nb;
    ber_sorted(i) = sort_nulling_cancelling_erros/Nb;
    
    %disp(zero_forcing_erros);
    %disp(nulling_cancelling_erros);
    %disp(sort_nulling_cancelling_erros);    
end


figure(k)
semilogy(Eb_N0_dB, ber_zero, Eb_N0_dB, ber_nulling, Eb_N0_dB, ber_sorted, 'LineWidth', 3);
grid on;

title(strcat('Taxa de erros para MIMO com NT:', mat2str(NT),' NR:',mat2str(NR)));
legend('Zero Forcing', 'Nulling Cancelling','Sorted Nulling Cancelling');
ylabel('BER');
xlabel('Eb/N0 (dB)');

end