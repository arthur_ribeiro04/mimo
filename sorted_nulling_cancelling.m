function resultado = sorted_nulling_cancelling(x, H, y)   
    norma_h = zeros(size(H, 2), 1);    
    % percorre as coluna da matriz H para c�lculo da norma
    for i = 1:size(H, 2)
        norma_h(i) = norm(H(:,i));        
    end    
    [norma_h_ord, I] = sort(norma_h);
    
    %I = I.';       
    
    for i = 1:size(I)
        H_ord(:,i) = H(:,I(i));
    end
    
    
    [Q,R] = qr(H_ord);
 
    % z = Rx+~n
    z = Q' * y;        
    
    resultado = zeros(size(H, 2), 1);    
    for i = size(H, 2):-1:1
        if (real(z(i) / R(i, i))) >= 0
            resultado(i)= 1;
        else
            resultado(i)=-1;
        end
        z = z - resultado(i) * R(:,i);
    end
    
    aux = resultado;
    for i = 1:size(I)
        resultado(I(i)) = aux(i);
    end
    
    %if x ~= resultado
    %disp("--------------------------");
    %disp (I');
    %disp (H);
    %disp(H_ord);
    %disp("--------------------------");
    %disp("********************")
    %disp (x);
    %disp (resultado);
    %disp("********************")
    %end
end

